GO
create procedure ListarCategorias
as
select id[Id],
	ISNULL(codigo,'')[Codigo],
	ISNULL(nombre,'')[Nombre],
	ISNULL(descripcion,'')[Descripcion]
FROM taller_mecanico.categoria;

GO
create procedure TraerCategoriaPorId
@ID int
as
SELECT id [Id],
	ISNULL(codigo,'')[Codigo],
	ISNULL(nombre,'')[Nombre],
	ISNULL(descripcion,'')[Descripcion]
from taller_mecanico.categoria
where id = @ID;


GO
create procedure InsertarCategoria
@CODIGO varchar(50),
@NOMBRE varchar(100),
@DESCRIPCION TEXT
AS
insert into 
taller_mecanico.categoria(codigo,nombre,descripcion)
values(@CODIGO,@NOMBRE,@DESCRIPCION);



GO
create procedure ActualizarCategoria
	@ID int,
	@CODIGO VARCHAR(50),
	@NOMBRE VARCHAR(50),
	@DESCRIPCION TEXT
AS
UPDATE taller_mecanico.categoria set codigo = @CODIGO,
	nombre = @NOMBRE, descripcion = @DESCRIPCION
WHERE id = @ID;


GO
create procedure EliminarCategoria
	@ID int
AS
DELETE FROM taller_mecanico.categoria
WHERE id = @ID;



